def calculate_determinant_2X2(matrix):
    if len(matrix) == 2:
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]


def multiplication_between_scalar_and_matrix(scalar, matrix):
    index_row = 0
    while index_row < len(matrix):
        index_column = 0
        while index_column < len(matrix[index_row]):
            matrix[index_row][index_column] = scalar * matrix[index_row][index_column]
            index_column = index_column + 1
        index_row = index_row + 1
    return matrix


def multiplication_between_key_and_matrix(key, matrix2x2):
    resultant_to_multiply = []
    for index_row in range(len(key)):
        for index_column in range(len(key[index_row])):
            if index_column % 2 == 0:
                valueAux = key[index_row][index_column] * matrix2x2[0]
            else:
                valueAux = key[index_row][index_column] * matrix2x2[1]
            resultant_to_multiply.append(valueAux)
    return sum_of_vector_components(resultant_to_multiply)


def sum_of_vector_components(matrix_to_sum):
    component_x, component_y = 0, 0
    for index in range(len(matrix_to_sum)):
        if index < 2:
            component_x += matrix_to_sum[index]
        else:
            component_y += matrix_to_sum[index]

    return [component_x, component_y]


def calculateModule(matrixToCalculate, size):
    matrix = []
    for index in range(len(matrixToCalculate)):
        matrix.append(matrixToCalculate[index] % size)
    return matrix


def do_euclides_algorithm(number, module):
    number = number % module
    for x in range(1, module):
        if (number * x) % module == 1:
            return x
    return 1


def get_size(image):
    width, height = image.size
    return width


def reverse_matrix(matrixToInvert, size):
    determinant = calculate_determinant_2X2(matrixToInvert)
    matrixToInvert = [[matrixToInvert[1][1], (-1 * matrixToInvert[0][1])],
                      [(-1 * matrixToInvert[1][0]), matrixToInvert[0][0]]]

    mcd = do_euclides_algorithm(determinant % size, size)
    matrixAux = multiplication_between_scalar_and_matrix(mcd, matrixToInvert)

    for i in range(len(matrixAux)):
        for j in range(len(matrixAux[i])):
            matrixAux[i][j] = matrixAux[i][j] % size

    return matrixAux


def validate_size(width, height):
    is_a_valid_size = False
    if width == height:
        is_a_valid_size = True
    return is_a_valid_size


def verifyIfMatrixIsReverse(matrix):
    if calculate_determinant_2X2(matrix) != 0:
        return True
    else:
        return False


def convertInMatrix(message):
    ALPHABET = "abcdefghijklmnopqrstuvwxyz"

    letter_to_index = dict(zip(ALPHABET, range(len(ALPHABET))))
    message_in_numbers = []

    for letter in message[0:4]:
        message_in_numbers.append(letter_to_index[letter])

    matrix = [[message_in_numbers[0], message_in_numbers[1]], [message_in_numbers[2], message_in_numbers[3]]]

    if verifyIfMatrixIsReverse(matrix):
        return matrix


def get_any_matrix(matrix):
    determinant = calculate_determinant_2X2(matrix)
    matrix_list = [[[21, 35], [18, 79]], [[4, -2], [3, -1]]]
    return matrix_list[determinant % len(matrix_list)]

