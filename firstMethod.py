import PIL.Image
from PIL import Image
import numpy as np

from Utils import multiplication_between_key_and_matrix, calculateModule, get_size, reverse_matrix, validate_size


class FirstMethod:
    def __init__(self, path):
        self.path = path
        self.keyMatrix = [[21, 35], [18, 79]]
        self.matrixImage = []

    def crop_image_rectangular(self):
        im1 = self.path.resize((256, 256), Image.ANTIALIAS)
        return im1

    def verify_photo(self):
        size = self.path.size
        width, height = size
        if validate_size(width, height):
            print("That's ok")
        else:
            self.path = self.crop_image_rectangular()

    def do_method_hill(self, matrix_to_encrypt, secretMatrix):
        i = 0
        while i < len(matrix_to_encrypt):
            j = 0
            while j < len(matrix_to_encrypt[i]) - 1:
                matrix = [matrix_to_encrypt[i][j], matrix_to_encrypt[i][j + 1]]
                matrix_aux = multiplication_between_key_and_matrix(secretMatrix, matrix)
                matrix_aux = calculateModule(matrix_aux, len(matrix_to_encrypt), 256)

                matrix_to_encrypt[i][j] = matrix_aux[0]
                matrix_to_encrypt[i][j + 1] = matrix_aux[1]
                j = j + 2
            i = i + 1

    def decipher_image_1(self, matrixImage, secretMatrixReverse):
        matrixAux = matrixImage.copy()

        self.do_method_hill(matrixAux, secretMatrixReverse)

        image1 = PIL.Image.fromarray(matrixAux)
        image1.show()

    def encryption_of_a_secret_image_using_hill_method(self):
        matrix_to_encrypt = self.matrixImage.copy()
        self.do_method_hill(matrix_to_encrypt, self.keyMatrix)

        image = PIL.Image.fromarray(matrix_to_encrypt)
        image.save('Images/ImagesWithEncryption/encryptedImageMethod1.jpg')
        image.show()
        self.decipher_image_1(matrix_to_encrypt, reverse_matrix(self.keyMatrix, get_size(image)))

    def convert_an_image_in_pixel_matrix_BN(self):
        image_in_bn = Image.open(self.path)
        self.matrixImage = np.asarray(image_in_bn)

    def convert_an_image_to_BN_image(self):
        self.path = Image.open(self.path)
        self.verify_photo()
        im = self.path
        im.show()
        imageInBN = im.convert('L')

        path = 'Images/ImagesWithoutEncryption/imageBN.jpg'
        imageInBN.save(path)
        imageInBN.show()

        return path

    def execute_method(self):
        self.path = self.convert_an_image_to_BN_image()
        self.convert_an_image_in_pixel_matrix_BN()
        self.encryption_of_a_secret_image_using_hill_method()


