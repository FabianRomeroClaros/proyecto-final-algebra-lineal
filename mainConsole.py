"""
 Participants:
 - Jorge Heredia
 - Jefersson Coronel
 - Juan Carlos Hidalgo
 - Fabian Romero
 - Santiago Caballero
"""

from firstMethod import FirstMethod
from secondMethod import SecondMethod


def showMenu():
    condition = True
    while condition:
        msg = str("=" * 100) + "\nEnter 1. If you want to cipher and decipher a picture using hill method" \
              "\nEnter 2. If you want to encrypt and decrypt an image using the pixel movement method" \
              "\nEnter 3. If you want to close the program\n => "

        num = int(input(msg))

        if num == 1:
            #doFirstMethod("Images/ImagesWithoutEncryption/image2.jpg")
            FirstMethod("Images/ImagesWithoutEncryption/ImagesForMethod1/image2.jpg").execute_method()
        elif num == 2:
            SecondMethod("Images/ImagesWithoutEncryption/ImagesForMethod2/image5.jpeg").executeMethod()
            #doSecondMethod("Images/ImagesWithoutEncryption/image5.jpeg")
        elif num == 3:
            condition = False
        else:
            print("Alert: Value not recognized.")
            showMenu()


if __name__ == '__main__':
    showMenu()
