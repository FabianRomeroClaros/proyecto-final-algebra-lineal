# PROYECTO FINAL DE ÁLGEBRA LINEAL

---

## Miembros:
- Caballero Santiago
- Coronel Jefersson
- Heredia Jorge
- Hidalgo Juan Carlos
- Romero Fabián

## Objetivo.-

---

- ##### Implementar en Python un programa que permita el cifrado y descifrado de una imagen utilizando ambos métodos.

---
## Descripcion

- ##### Programa para cifrar y descifrar textos clave en imagenes mediante el <em>**Metodo de Hill**</em> y el <em>**Movimiento de Pixeles**</em>
---
## Flujo:

- El programa cuenta con dos formas de correr una por interfaz grafica y otra por consola
- En el caso de optar por la UI nos apareceran 2 opciones:

### Por UI:

---

> #### Metodos para escoger:
>
>> - Cifrado y descifrado de una imagen secreta mediante el **Metodo de Hill**
>> - CIfrado y descifrado de una imagen secreta mediante el **Movimiento de Pixeles**
>> 

<img alt="Menu" src="Images/ImagesForReadme/Menu.png">

> #### Cifrado y descifrado de una imagen secreta mediante el **Metodo de Hill**

-  En el caso que se elija este metodo la siguiente pantalla sera para insertar la clave o el texto para cifrar

<img alt="ask" src="Images/ImagesForReadme/askKeyUI.png">

- Una vez insertado el texto clave nos pedira seleccionar una imagen para realizar el proceso de cifrado y descrifrado

<img alt="ask" src="Images/ImagesForReadme/chooseImg.png">

- <p> Enseguida el programa cifrara el texto clave mediante el metodo de Hill en la imagen y luego lo descifrara para demostrar que el metodo funciona para el cifrado y la matriz inversa para el descifrado </p>

<img alt="demostration" src="Images/ImagesForReadme/example1.png">


> #### CIfrado y descifrado de una imagen secreta mediante el **Movimiento de Pixeles**

-  En el caso que se elija este metodo la siguiente pantalla sera para seleccionar una imagen para realizar el proceso de cifrado y descrifrado mediante el metodo de movimiento de pixeles

~~~
do_method_by_moving_pixels(self, matrixAux, secretMatrix, matrixImage)
~~~

- Una vez cifrada la imagen se procede a descifrar para mostrar en pantalla como en el ejemplo:

<img alt="demostration1" src="Images/ImagesForReadme/example2.png">

---
### Por Consola:

- Nos aparecen 3 opciones :

> #### Opciones para escoger:
>
>> - **Enter 1.** If you want to graph the function level curves in 2 dimensions
>> - **Enter 2.** If you want to graph multiple vectors to represent an electromagnetic field
>> - **Enter 3.** If you want to close the program
>>
#### Opcion 1.-
- La primera opcion nos pasara al metodo de Hill y se seguira el mismo proceso mencionado para el cifrado y descifrado de la imagen

<img alt="demostration1" src="Images/ImagesForReadme/example1.png">

#### Opcion 2.-
- La segunda opcion nos pasara al metodo por movimiento de pixeles y se seguira el mismo proceso mencionado anteriormente para el cifrado y descifrado de la imagen

<img alt="demostration1" src="Images/ImagesForReadme/exampleConsole.png">

#### Opcion 3.-
- Cierra o finaliza el programa





