import PIL.Image
from PIL import Image
import numpy as np

from Utils import multiplication_between_key_and_matrix, calculateModule, reverse_matrix, validate_size


class SecondMethod:

    def __init__(self, path):
        self.path = path
        self.keyMatrix = [[21, 35], [18, 79]]
        self.matrixImage = []

    def crop_image_rectangular(self):
        width, height = self.path.size

        if width > height:
            left = 0
            top = 0
            right = height
            bottom = height
        else:
            left = 0
            top = 0
            right = width
            bottom = width

        im1 = self.path.crop((left, top, right, bottom))
        return im1

    def verify_photo(self):
        size = self.path.size
        width, height = size
        if validate_size(width, height):
            print("That's ok")
        else:
            self.path = self.crop_image_rectangular()

    def decipher_image_2(self, matrixImage, secretMatrixReverse):
        matrixAux = matrixImage.copy()
        self.method_by_moving_pixels(matrixAux, secretMatrixReverse, matrixImage)
        image1 = PIL.Image.fromarray(matrixAux)
        image1.show()

    def method_by_moving_pixels(self, matrixAux, secretMatrix, matrixImage):
        for i in range(len(matrixAux)):
            for j in range(len(matrixAux[i])):
                matrix = [i, j]
                x = multiplication_between_key_and_matrix(secretMatrix, matrix)
                m = calculateModule(x, len(matrixImage))
                matrixAux[i][j] = matrixImage[m[0]][m[1]]

    # F
    def encryption_of_a_secret_image_by_moving_pixels(self):
        matrixAux = self.matrixImage.copy()

        self.method_by_moving_pixels(matrixAux, self.keyMatrix, self.matrixImage)

        image = PIL.Image.fromarray(matrixAux)
        image.save('Images/ImagesWithEncryption/encryptedImageMethod2.jpg')
        image.show()

        self.decipher_image_2(matrixAux, reverse_matrix(self.keyMatrix, len(matrixAux)))

    def convert_an_image_in_pixel_matrix_Color(self):
        self.path = Image.open(self.path)
        self.verify_photo()
        self.path.show()
        self.matrixImage = np.asarray(self.path)

    def executeMethod(self):
        self.convert_an_image_in_pixel_matrix_Color()
        self.encryption_of_a_secret_image_by_moving_pixels()
