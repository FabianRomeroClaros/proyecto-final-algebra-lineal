from PyQt5.QtWidgets import QWidget, QFileDialog


class ImgReader(QWidget):
    def __init__(self):
        super().__init__()
        self.filePath = ""
        self.title = 'Image Picker'
        self.left = 500
        self.top = 230
        self.width = 1000
        self.height = 1000
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.openFileNameDialog()
        self.show()

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.filePath, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                          "Jpg Files (*.jpg);;Jpeg FIle (*.jpeg);;Png Files (*.png)", options=options)
