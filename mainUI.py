import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QLabel, QPushButton, QLineEdit, QMessageBox
from PyQt5.QtGui import QFont
from PyQt5.QtCore import pyqtSlot

import Utils
from fileReader import ImgReader
from firstMethod import FirstMethod
from secondMethod import SecondMethod

buttonStyleSheet = """
        QPushButton {
            background-color: #16a085; 
            border-radius: 20; 
            color: #fff; 
        }
        QPushButton:hover {
            color: #ddd;
            background-color: #149174; 
            border: 2px solid white;
        }
    """
titleStyleSheet = "color: #fff; border: 2px solid white; padding: 20; border-radius: 8;"
titleStyleSheet1 = "color: #fff;"
windowStyleSheet = "background-color: #1a1a1a;"
textFieldStyleSheet = "border: 1px solid white; color: #fff; padding: 10; border-radius: 12"
messageStyle = """
QMessageBox {
    background-color: rgb(51, 51, 51);
}

QMessageBox QLabel {
    color: rgb(200, 200, 200);
}
"""


class Menu(QMainWindow):

    def __init__(self):
        super().__init__()
        self.option1 = QPushButton('Encryption and decryption \n of an image using  \nmethod Hill', self)
        self.option2 = QPushButton(
            'Encryption and decryption \n of a secret image \n by moving pixels \n', self
        )
        self.label = QLabel("Image Encryptor", self)
        self.title = 'Main Pane Encryptor'
        self.left = 550
        self.top = 230
        self.width = 1000
        self.height = 700
        self.initUI()
        self.userKey = ""
        self.textbox = QLineEdit(self)
        self.button = QPushButton('Send', self)
        self.label1 = QLabel("Enter the password: ", self)

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setStyleSheet(windowStyleSheet)

        # Texto
        self.label.move(400, 100)
        self.label.resize(280, 100)
        self.label.setFont(QFont('Arial', 26))
        self.label.setStyleSheet(titleStyleSheet)

        # Botón para mandar la clave introducida
        self.option1.move(80, 280)
        self.option1.resize(400, 200)
        self.option1.clicked.connect(self.on_click_option1)
        self.option1.setStyleSheet(buttonStyleSheet)
        self.option1.setFont(QFont('Arial', 18))

        self.option2.move(520, 280)
        self.option2.resize(400, 200)
        self.option2.clicked.connect(self.on_click_option2)
        self.option2.setStyleSheet(buttonStyleSheet)
        self.option2.setFont(QFont('Arial', 18))

        self.show()

    def initTextField(self):
        # Texto
        self.label1.move(280, 240)
        self.label1.resize(500, 40)
        self.label1.setFont(QFont('Arial', 18))
        self.label1.setStyleSheet(titleStyleSheet1)
        self.label1.show()

        # Textfield donde el usuario introduzca la clave de encriptado
        self.textbox.move(280, 300)
        self.textbox.resize(500, 40)
        self.textbox.setStyleSheet(textFieldStyleSheet)
        self.textbox.show()

        # Botón para mandar la clave introducida
        self.button.setFont(QFont('Arial', 15))
        self.button.move(460, 370)
        self.button.clicked.connect(self.on_click)
        self.button.setStyleSheet(buttonStyleSheet)
        self.button.show()

    @pyqtSlot()
    def on_click_option1(self):
        self.label.hide()
        self.option1.hide()
        self.option2.hide()
        self.initTextField()

    @pyqtSlot()
    def on_click_option2(self):
        self.getImagePath()

    def mainMenu(self):
        self.label1.hide()
        self.textbox.hide()
        self.button.hide()
        self.label.show()
        self.option1.show()
        self.option2.show()

    def getImagePath(self):
        SecondMethod(ImgReader().filePath).executeMethod()

    @pyqtSlot()
    def on_click(self):
        self.userKey = self.textbox.text()
        if len(self.userKey) >= 4:
            QMessageBox.question(
                self,
                'Great',
                'The key was sent successfully. Choose an image to perform the encryption and decryption processLa clave ',
                QMessageBox.Ok
            )
            self.textbox.setText("")
            self.mainMenu()
            FirstMethod(ImgReader().filePath).execute_method()

        else:
            QMessageBox.question(
                self,
                'Error',
                'Your secret string must have a number of letters greater than 4',
                QMessageBox.Abort
            )


if __name__ == '__main__':
    app = QApplication(sys.argv)
    s = Menu()
    sys.exit(app.exec_())
